/**
* Sample React Native App
* https://github.com/facebook/react-native
* @flow
*/

import React, { Component } from 'react';
import {
Platform,
StyleSheet,
Text,
View,
Switch,
Image,
PixelRatio,
KeyboardAvoidingView,
ScrollView,
TouchableHighlight,
TouchableOpacity
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Carousel, { Pagination,ParallaxImage } from 'react-native-snap-carousel';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import { NavigationActions } from 'react-navigation';
const Dimensions = require('Dimensions');
const {height, width} = Dimensions.get('window');
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import  Entypo from 'react-native-vector-icons/Entypo'
import EvilIcons from 'react-native-vector-icons/EvilIcons'
import Feather from 'react-native-vector-icons/Feather'

import SplashScreen from 'react-native-smart-splash-screen'

import styles from './styles.js'
import { ENTRIES1 } from './entries';
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

function wp (percentage) {
  const value = (percentage * viewportWidth) / 100;
  return Math.round(value);
}

const slideHeight = viewportHeight * 0.4;
const slideWidth = wp(90);
const itemHorizontalMargin = wp(2);
const sliderWidth = viewportWidth;
const itemWidth = slideWidth + itemHorizontalMargin * 2;


export default class WorkoutDetails extends Component<{}> {
  constructor(props) {
    super(props);
    this.state = {
      width:0,
      height : 0,
      slider1Ref :null,
      slider1ActiveSlide: 1,
    };
  }

  _renderWorkout= ({item, index},parallaxProps)=> {

    return (
        <View >
          <TouchableOpacity
              activeOpacity={1}
              style={styles.carousel_style_workout}
              onPress={() => { alert('hi'); }}
              >
                <View style={styles.carousel_image_view}>

                    <ParallaxImage
                    source={{ uri: item.illustration }}
                    containerStyle={{flex: 1,backgroundColor: 'white',}}
                    style={styles.carousel_image}
                    parallaxFactor={0.35}
                    showSpinner={true}
                    spinnerColor={'rgba(0, 0, 0, 0.25)'}
                    {...parallaxProps}
            />

                </View>
                <View style={styles.carousel_text_view}>
                    <Text
                      style={styles.carousel_text}
                      numberOfLines={2}
                    >
                      some text
                    </Text>

                </View>
            </TouchableOpacity>
        </View>
    );
}


    _renderTrainers= ({item, index},parallaxProps)=> {

      return (
          <View >
            <TouchableOpacity
                activeOpacity={1}
                style={styles.carousel_style_trainers}
                onPress={() => { alert('hi'); }}
                >
                  <View style={styles.carousel_image_view}>

                      <ParallaxImage
                      source={{ uri: item.illustration }}
                      containerStyle={{flex: 1,backgroundColor: 'white',}}
                      style={styles.carousel_image}
                      parallaxFactor={0.35}
                      showSpinner={true}
                      spinnerColor={'rgba(0, 0, 0, 0.25)'}
                      {...parallaxProps}
              />

                  </View>
                  <View style={styles.carousel_text_view}>
                      <Text
                        style={styles.carousel_text}
                        numberOfLines={2}
                      >
                      some text
                      </Text>

                  </View>
              </TouchableOpacity>
          </View>
      );
  }


      _renderPlans= ({item, index},parallaxProps)=> {

        return (
            <View >
              <TouchableOpacity
                  activeOpacity={1}
                  style={styles.carousel_style_plans}
                  onPress={() => { alert('hi'); }}
                  >
                    <View style={styles.carousel_image_view}>

                        <ParallaxImage
                        source={{ uri: item.illustration }}
                        containerStyle={{flex: 1,backgroundColor: 'white',}}
                        style={styles.carousel_image}
                        parallaxFactor={0.35}
                        showSpinner={true}
                        spinnerColor={'rgba(0, 0, 0, 0.25)'}
                        {...parallaxProps}
                />

                    </View>
                    <View style={styles.carousel_text_view}>
                        <Text
                          style={styles.carousel_text}
                          numberOfLines={2}
                        >
                          some text
                        </Text>

                    </View>
                </TouchableOpacity>
            </View>
        );
    }


render() {
  const {navigation} = this.props
  const resizeMode = 'center';
  const text = 'I am some centered text';
  return (

    <View style={styles.mainBody} >


          <ScrollView
            style={{flex:1}}
            contentContainerStyle={{paddingBottom: 50}}
            indicatorStyle={'white'}
            scrollEventThrottle={200}
            directionalLockEnabled={true}
          >
            <View style={styles.listofWrkouts21}>
              <View style={styles.chevron_left_icon}>
                <TouchableOpacity onPress={()=>this.onPressChevronLeft(navigation )}>
                    <FontAwesome name="chevron-left" size={25} color="#FF7E00"   />
                </TouchableOpacity>
              </View>
              <View style={styles.header_wrkout_details}>
                     <Image style={{width: Dimensions.get('window').width, height:260 }} source={{uri:'wrkout_details_banner'}} />

              </View>
              <View>
                  <Text style={styles.wrkdetails_name1}>
                    Hey Sophie!
                  </Text>
                  <View style={styles.wrkdetails_name1abc_mas}>
                    <Text style={styles.wrkdetails_name1a}> 60 MIN!</Text>
                    <Text style={styles.wrkdetails_name1b}> HARD LEVEL</Text>
                    <Text style={styles.wrkdetails_name1c}>SILENT WORKOUT</Text>
                  </View>
              </View>




            </View>





          </ScrollView>




          <View style={styles.footer}>

              <TouchableOpacity onPress = {()=>this.onPressSignin(navigation )}>
                <View style={styles.save_view}>
                    <Text style={styles.save_btnTxt}>Start Workout</Text>
                </View>

                <Image
                  style={styles.save_btnImg}
                  source={{ uri: 'buttonimg' }}
                />

              </TouchableOpacity>
            </View>



          </View>

  );
}
}
