/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Switch,
  Image,
  PixelRatio,
  KeyboardAvoidingView,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  TextInput

} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation';

const Dimensions = require('Dimensions');
const {height, width} = Dimensions.get('window');

import SplashScreen from 'react-native-smart-splash-screen'
import styles from './styles.js'



export default class SignUp extends Component<{}> {
  constructor(props) {
  super(props);
  this.state = {
    width:0,
    height : 0
  };
}
componentDidMount () {


}

  onPressChevronLeft = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'Home',
      params: {'Home':'yes'},
      })
    navigate.dispatch(navigateAction)
  }

  onPressLogin = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'UpcomingWorkouts',
      params: {'UpcomingWorkouts':'yes'},
      })
    navigate.dispatch(navigateAction)
  }



// <View>
//     <Icon name="chevron-left" size={30} color="#900" style={{marginLeft:16,marginTop:16,position:'absolute'}} />
// </View>
  render() {
    const {navigation} = this.props
    const resizeMode = 'center';
    const text = 'I am some centered text';
    const myIcon = (<Icon name="rocket" size={30} color="#900" />)
    console.log(styles)
    return (


            <View style={styles.mainBody}>
              <View style={styles.chevron_left_icon}>
                <TouchableOpacity onPress={()=>this.onPressChevronLeft(navigation )}>
                  <Icon name="chevron-left" size={25} color="#FF7E00"   />
                </TouchableOpacity>
              </View>
              <View style={styles.header}>
                    <Text style={styles.topSignupTxt}>
                      User info 1/2
                    </Text>
              </View>

              <ScrollView
                style={{
                  marginTop : 20,
                  marginBottom:0,
                  paddingBottom:60,

                  width:Dimensions.get('window').width
                }}

              >
              <KeyboardAvoidingView behavior="padding">
              <View style={styles.signup_temp_form}>
              <View style={styles.body1a}>
                    <TextInput  placeholder="Firssdt Name" underlineColorAndroid='transparent' autoCorrect={false} placeholderTextColor='#626264' style={styles.textInput_signup_new1} />
                    <TextInput  placeholder="Last Name" underlineColorAndroid='transparent' autoCorrect={false} placeholderTextColor='#626264' style={[styles.textInput_signup_new1, styles.textInput_signup_new2]} />
                    <TextInput  placeholder="Username" underlineColorAndroid='transparent' autoCorrect={false} placeholderTextColor='#626264' style={[styles.textInput_signup_new1, styles.textInput_signup_new2]} />
               </View>
              </View>
              </KeyboardAvoidingView>
              </ScrollView>


              <View style={styles.footer}>

                  <TouchableOpacity onPress={()=>this.onPressLogin(navigation )}>
                    <View style={styles.save_view}>
                        <Text style={styles.save_btnTxt}>Save and continue</Text>
                    </View>

                    <Image
                      style={styles.save_btnImg}
                      source={{ uri: 'buttonimg' }}
                    />

                  </TouchableOpacity>
                </View>

                  </View>



    );
  }
}
