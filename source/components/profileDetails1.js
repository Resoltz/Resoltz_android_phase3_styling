/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Switch,
  Image,
  PixelRatio,
  KeyboardAvoidingView,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  TextInput

} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation';

const Dimensions = require('Dimensions');
const {height, width} = Dimensions.get('window');

import SplashScreen from 'react-native-smart-splash-screen'
import styles from './styles.js'
import {Slider} from 'react-native-elements'



export default class ProfileDetails1 extends Component<{}> {
  constructor(props) {
  super(props);
  this.state = {
    width:0,
    height : 0
  };
}
componentDidMount () {


}

  onPressChevronLeft = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'Home',
      params: {'Home':'yes'},
      })
    navigate.dispatch(navigateAction)
  }

  onPressLogin = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'UpcomingWorkouts',
      params: {'UpcomingWorkouts':'yes'},
      })
    navigate.dispatch(navigateAction)
  }



// <View>
//     <Icon name="chevron-left" size={30} color="#900" style={{marginLeft:16,marginTop:16,position:'absolute'}} />
// </View>
  render() {
    const {navigation} = this.props
    const resizeMode = 'center';
    const text = 'I am some centered text';
    const myIcon = (<Icon name="rocket" size={30} color="#900" />)
    console.log(styles)
    return (


            <View style={styles.mainBody}>
              <View style={styles.chevron_left_icon}>
                <TouchableOpacity onPress={()=>this.onPressChevronLeft(navigation )}>
                  <Icon name="chevron-left" size={25} color="#FF7E00"   />
                </TouchableOpacity>
              </View>
              <View style={styles.header}>
                    <Text style={styles.topSignupTxt}>
                      Profile Details (1/2)
                    </Text>
              </View>

                       <TouchableOpacity onPress={()=>this.onPressLogin(navigation )} style={styles.touch_align1a}>
                       <View style={styles.circle1_mas1}>
                         <View style={styles.userinfo_2}>
                            <Text style={styles.user_info_btn2  }>Pick school</Text>
                            <Icon name="chevron-right" size={25} style={styles.user_info_Arrow}   />
                         </View>
                       </View>
                       </TouchableOpacity>
                       <View style={styles.slider_mas}>
                          <Text style={styles.genderText  }>Gender Identity</Text>
                         <View  style={styles.genderSlider_mas  }>
                            <Text  style={styles.maleGender  }>Male</Text>
                            <Slider   style={styles.genderSlider  }
                                 style = {{width:'40%'}}
                                 step={1}
                                 minimumValue={0}
                                 maximumValue={2}
                                 thumbTouchSize = {{width: 80, height: 80}}
                                 thumbTintColor = 'orange'
                                  />
                            <Text  style={styles.femaleGender  }>Female</Text>
                         </View>
                       </View>
                       <View style={styles.slider_mas2}>
                          <Text style={styles.genderText  }>Gender at birth (BMR calc)</Text>
                         <View  style={styles.genderSlider  }>
                            <Text  style={styles.maleGender  }>Male</Text>
                            <Slider   style={styles.genderSlider  }
                                 style = {{width:'40%'}}
                                 step={1}
                                 minimumValue={0}
                                 maximumValue={2}
                                 thumbTouchSize = {{width: 80, height: 80}}
                                 thumbTintColor = 'orange'
                                  />
                            <Text  style={styles.femaleGender  }>Female</Text>
                         </View>
                       </View>



              <View style={styles.footer}>

                  <TouchableOpacity onPress={()=>this.onPressLogin(navigation )}>
                    <View style={styles.save_view}>
                        <Text style={styles.save_btnTxt}>Save and continue</Text>
                    </View>

                    <Image
                      style={styles.save_btnImg}
                      source={{ uri: 'buttonimg' }}
                    />

                  </TouchableOpacity>
                </View>

                  </View>



    );
  }
}
