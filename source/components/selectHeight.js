/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Switch,
  Image,
  PixelRatio,
  KeyboardAvoidingView,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  TextInput,
  Picker
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation';

const Dimensions = require('Dimensions');
const {height, width} = Dimensions.get('window');

import SplashScreen from 'react-native-smart-splash-screen'
import styles from './styles.js'
import {Slider} from 'react-native-elements'



export default class SelectHeight extends Component<{}> {
  constructor(props) {
  super(props);
  this.state = {
    width:0,
    height : 0
  };
}
componentDidMount () {


}

  onPressChevronLeft = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'Home',
      params: {'Home':'yes'},
      })
    navigate.dispatch(navigateAction)
  }

  onPressLogin = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'UpcomingWorkouts',
      params: {'UpcomingWorkouts':'yes'},
      })
    navigate.dispatch(navigateAction)
  }



// <View>
//     <Icon name="chevron-left" size={30} color="#900" style={{marginLeft:16,marginTop:16,position:'absolute'}} />
// </View>
  render() {
    const {navigation} = this.props
    const resizeMode = 'center';
    const text = 'I am some centered text';
    const myIcon = (<Icon name="rocket" size={30} color="#900" />)
    console.log(styles)
    return (


            <View style={styles.mainBody}>
              <View style={styles.chevron_left_icon}>
                <TouchableOpacity onPress={()=>this.onPressChevronLeft(navigation )}>
                  <Icon name="chevron-left" size={25} color="#FF7E00"   />
                </TouchableOpacity>
              </View>
              <View style={styles.header}>
                    <Text style={styles.topSignupTxt}>
                        Select Height
                    </Text>
              </View>


              <Picker style = {{color:'white'}}>
                <Picker.Item label="1 feet" value="1"/>
                <Picker.Item label="2 feet" value="2"/>
                <Picker.Item label="3 feet" value="3"/>
                <Picker.Item label="4 feet" value="4"/>
                <Picker.Item label="5 feet" value="5"/>
                <Picker.Item label="6 feet" value="6"/>
                <Picker.Item label="7 feet" value="7"/>
                <Picker.Item label="8 feet" value="8"/>
                <Picker.Item label="9 feet" value="9"/>
                <Picker.Item label="10 feet" value="10"/>
                <Picker.Item label="11 feet" value="11"/>
                <Picker.Item label="12 feet" value="12"/>
              </Picker>
              <Picker style = {{color:'white'}}>
                  <Picker.Item label="1 inches" value="1"  />
                  <Picker.Item label="2 inches" value="2"/>
                  <Picker.Item label="3 inches" value="3"/>
                  <Picker.Item label="4 inches" value="4"/>
                  <Picker.Item label="5 inches" value="5"/>
                  <Picker.Item label="6 inches" value="6"/>
                  <Picker.Item label="7 inches" value="7"/>
                  <Picker.Item label="8 inches" value="8"/>
                  <Picker.Item label="9 inches" value="9"/>
                  <Picker.Item label="10 inches" value="10"/>
                  <Picker.Item label="11 inches" value="11"/>
                  <Picker.Item label="12 inches" value="12"/>
                </Picker>
                <Picker style = {{color:'white'}}>
                  <Picker.Item label="Imperial" value="1" color="#fff"/>
                </Picker>




              <View style={styles.footer}>

                  <TouchableOpacity onPress={()=>this.onPressLogin(navigation )}>
                    <View style={styles.save_view}>
                        <Text style={styles.save_btnTxt}>Save and continue</Text>
                    </View>

                    <Image
                      style={styles.save_btnImg}
                      source={{ uri: 'buttonimg' }}
                    />

                  </TouchableOpacity>
                </View>

                  </View>



    );
  }
}
