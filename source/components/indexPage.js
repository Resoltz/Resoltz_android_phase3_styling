/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Switch,
  Image,
  PixelRatio,
  KeyboardAvoidingView,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  TextInput

} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation';

const Dimensions = require('Dimensions');
const {height, width} = Dimensions.get('window');

import SplashScreen from 'react-native-smart-splash-screen'
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import  Entypo from 'react-native-vector-icons/Entypo'
import EvilIcons from 'react-native-vector-icons/EvilIcons'
import Feather from 'react-native-vector-icons/Feather'
import Ionicons from 'react-native-vector-icons/Ionicons'

import { TabNavigator } from 'react-navigation';


import styles from './styles.js'



export default class IndexPage extends Component<{}> {
  constructor(props) {
    super(props);
    this.state = {
      width:0,
      height : 0
    };
  }
  componentDidMount () {


  }

  onPress1 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'Youarefinished',
      params: {'Youarefinished':'yes'},
      })
    navigate.dispatch(navigateAction)
  }

  onPress2 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'GetNotified',
      params: {'GetNotified':'yes'},
      })
    navigate.dispatch(navigateAction)
  }
  onPress3 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'DevicesConnected',
      params: {'DevicesConnected':'yes'},
      })
    navigate.dispatch(navigateAction)
  }
  onPress4 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'WorkoutSummary',
      params: {'WorkoutSummary':'yes'},
      })
    navigate.dispatch(navigateAction)
  }
  onPress5 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'UpcomingWorkouts',
      params: {'UpcomingWorkouts':'yes'},
      })
    navigate.dispatch(navigateAction)
  }
  onPress6 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'Feed',
      params: {'Feed':'yes'},
      })
    navigate.dispatch(navigateAction)
  }
  onPress7 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'WorkoutDetails',
      params: {'WorkoutDetails':'yes'},
      })
    navigate.dispatch(navigateAction)
  }
  onPress8 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'UpcomingWorkouts8',
      params: {'UpcomingWorkouts8':'yes'},
      })
    navigate.dispatch(navigateAction)
  }
  onPress9 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'Home',
      params: {'Home':'yes'},
      })
    navigate.dispatch(navigateAction)
  }
  onPress10 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'SignIn',
      params: {'SignIn':'yes'},
      })
    navigate.dispatch(navigateAction)
  }
  onPress11 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'SignUp',
      params: {'SignUp':'yes'},
      })
    navigate.dispatch(navigateAction)
  }

  onPress12 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'Workout',
      params: {'Workout':'yes'},
      })
    navigate.dispatch(navigateAction)
  }

  onPress13 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'SchoolSignUp',
      params: {'SchoolSignUp':'yes'},
      })
    navigate.dispatch(navigateAction)
  }

  onPress14 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'UserInfo1',
      params: {'UserInfo1':'yes'},
      })
    navigate.dispatch(navigateAction)
  }

  onPress15 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'UserInfo2',
      params: {'UserInfo2':'yes'},
      })
    navigate.dispatch(navigateAction)
  }

  onPress16 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'UserInfo3',
      params: {'UserInfo3':'yes'},
      })
    navigate.dispatch(navigateAction)
  }

  onPress17 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'UserInfo4',
      params: {'UserInfo4':'yes'},
      })
    navigate.dispatch(navigateAction)
  }

  onPress18 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'Activity',
      params: {'Activity':'yes'},
      })
    navigate.dispatch(navigateAction)
  }

  onPress19 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'Calendar',
      params: {'Calendar':'yes'},
      })
    navigate.dispatch(navigateAction)
  }

  onPress20 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'Comments',
      params: {'Comments':'yes'},
      })
    navigate.dispatch(navigateAction)
  }

  onPress21 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'Filter',
      params: {'Filter':'yes'},
      })
    navigate.dispatch(navigateAction)
  }

  onPress22 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'FindFriends',
      params: {'FindFriends':'yes'},
      })
    navigate.dispatch(navigateAction)
  }

  onPress23 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'Menu',
      params: {'Menu':'yes'},
      })
    navigate.dispatch(navigateAction)
  }

  onPress24 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'Notifications',
      params: {'Notifications':'yes'},
      })
    navigate.dispatch(navigateAction)
  }


  onPress25 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'PickAnExercise',
      params: {'PickAnExercise':'yes'},
      })
    navigate.dispatch(navigateAction)
  }

  onPress26 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'PickCourse1',
      params: {'PickCourse1':'yes'},
      })
    navigate.dispatch(navigateAction)
  }

  onPress27 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'PickCourse2',
      params: {'PickCourse2':'yes'},
      })
    navigate.dispatch(navigateAction)
  }

  onPress28 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'PickCourse3',
      params: {'PickCourse3':'yes'},
      })
    navigate.dispatch(navigateAction)
  }

  onPress29 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'PickCourse4',
      params: {'PickCourse4':'yes'},
      })
    navigate.dispatch(navigateAction)
  }

  onPress30 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'Profile1',
      params: {'Profile1':'yes'},
      })
    navigate.dispatch(navigateAction)
  }

  onPress31 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'Profile2',
      params: {'Profile2':'yes'},
      })
    navigate.dispatch(navigateAction)
  }

  onPress32 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'Profile3',
      params: {'Profile3':'yes'},
      })
    navigate.dispatch(navigateAction)
  }

  onPress33 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'Profile4',
      params: {'Profile4':'yes'},
      })
    navigate.dispatch(navigateAction)
  }

  onPress34 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'ProfileDetails1',
      params: {'ProfileDetails1':'yes'},
      })
    navigate.dispatch(navigateAction)
  }

  onPress35 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'ProfileDetails2',
      params: {'ProfileDetails2':'yes'},
      })
    navigate.dispatch(navigateAction)
  }


  onPress36 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'ProfileDetails3',
      params: {'ProfileDetails3':'yes'},
      })
    navigate.dispatch(navigateAction)
  }


  onPress37 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'ProfileDetails4',
      params: {'ProfileDetails4':'yes'},
      })
    navigate.dispatch(navigateAction)
  }


  onPress38 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'PickSchool',
      params: {'PickSchool':'yes'},
      })
    navigate.dispatch(navigateAction)
  }


  onPress39 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'SchoolSignUp',
      params: {'SchoolSignUp':'yes'},
      })
    navigate.dispatch(navigateAction)
  }

  onPress40 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'SelectDOB',
      params: {'SelectDOB':'yes'},
      })
    navigate.dispatch(navigateAction)
  }

  onPress41 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'SelectHeight',
      params: {'SelectHeight':'yes'},
      })
    navigate.dispatch(navigateAction)
  }

  onPress42 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'SelectWeight',
      params: {'SelectWeight':'yes'},
      })
    navigate.dispatch(navigateAction)
  }

  onPress43 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'TrackActivity1',
      params: {'TrackActivity1':'yes'},
      })
    navigate.dispatch(navigateAction)
  }

  onPress44 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'TrackActivity2',
      params: {'TrackActivity2':'yes'},
      })
    navigate.dispatch(navigateAction)
  }

  onPress45 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'TrackActivity3',
      params: {'TrackActivity3':'yes'},
      })
    navigate.dispatch(navigateAction)
  }

  onPress46 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'TrackActivity4',
      params: {'TrackActivity4':'yes'},
      })
    navigate.dispatch(navigateAction)
  }

  onPress47 = (navigate) => {
    const navigateAction = NavigationActions.navigate({

      routeName: 'TrackActivity5',
      params: {'TrackActivity5':'yes'},
      })
    navigate.dispatch(navigateAction)
  }

  render() {
    const {navigation} = this.props

    return (
      <View style={styles.upcomingWorkouts_main_body}>

              <View style={styles.upcomingWorkouts_header}>
              <Text style={styles.upcomingWorkouts_heading}>
                  INDEX
              </Text>
              </View>

              <ScrollView style={styles.upcomingWorkouts_scrollView}>

                      <View style={styles.upcomingWorkouts_content_view}>
                          <View style={styles.upcomingWorkouts_content}>



                              <TouchableOpacity onPress = {()=>this.onPress1(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                you are finished
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}>
                              </View>

                              <TouchableOpacity onPress = {()=>this.onPress2(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                get notified
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>


                              <TouchableOpacity onPress = {()=>this.onPress3(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                devices connected
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>


                              <TouchableOpacity onPress = {()=>this.onPress4(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                workout summary
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>


                              <TouchableOpacity onPress = {()=>this.onPress5(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                upcoming workouts
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>


                              <TouchableOpacity onPress = {()=>this.onPress6(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                feed
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>


                              <TouchableOpacity onPress = {()=>this.onPress7(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                workout details
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>


                              <TouchableOpacity onPress = {()=>this.onPress8(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                connect device
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>


                              <TouchableOpacity >
                              <Text  style={styles.tuesday_heading}>
                                home
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>


                              <TouchableOpacity onPress = {()=>this.onPress10(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                SignIn
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>


                              <TouchableOpacity onPress = {()=>this.onPress11(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                SignUp
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>

                              <TouchableOpacity onPress = {()=>this.onPress12(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                Workout
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>

                              <TouchableOpacity onPress = {()=>this.onPress13(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                SchoolSignUp
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>

                              <TouchableOpacity onPress = {()=>this.onPress14(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                Userinfo1
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>

                              <TouchableOpacity onPress = {()=>this.onPress15(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                Userinfo2
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>

                              <TouchableOpacity onPress = {()=>this.onPress16(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                Userinfo3
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>

                              <TouchableOpacity onPress = {()=>this.onPress17(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                Userinfo4
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>

                              <TouchableOpacity onPress = {()=>this.onPress18(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                Activity
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>

                              <TouchableOpacity onPress = {()=>this.onPress19(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                Calendar
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>

                              <TouchableOpacity onPress = {()=>this.onPress20(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                Comments
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>

                              <TouchableOpacity onPress = {()=>this.onPress21(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                Filter
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>

                              <TouchableOpacity onPress = {()=>this.onPress22(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                FindFriends
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>


                              <TouchableOpacity onPress = {()=>this.onPress23(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                Menu
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>

                              <TouchableOpacity onPress = {()=>this.onPress24(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                Notifications
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>

                              <TouchableOpacity onPress = {()=>this.onPress25(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                PickAnExercise
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>

                              <TouchableOpacity onPress = {()=>this.onPress26(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                PickCourse1
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>

                              <TouchableOpacity onPress = {()=>this.onPress27(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                PickCourse2
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>

                              <TouchableOpacity onPress = {()=>this.onPress28(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                PickCourse3
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>

                              <TouchableOpacity onPress = {()=>this.onPress29(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                PickCourse4
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>

                              <TouchableOpacity onPress = {()=>this.onPress30(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                Profile1
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>

                              <TouchableOpacity onPress = {()=>this.onPress31(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                Profile2
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>

                              <TouchableOpacity onPress = {()=>this.onPress32(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                Profile3
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>

                              <TouchableOpacity onPress = {()=>this.onPress33(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                Profile4
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>

                              <TouchableOpacity onPress = {()=>this.onPress34(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                ProfileDetails1
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>

                              <TouchableOpacity onPress = {()=>this.onPress35(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                ProfileDetails2
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>

                              <TouchableOpacity onPress = {()=>this.onPress36(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                ProfileDetails3
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>

                              <TouchableOpacity onPress = {()=>this.onPress37(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                ProfileDetails4
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>

                              <TouchableOpacity onPress = {()=>this.onPress38(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                PickSchool
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>

                              <TouchableOpacity onPress = {()=>this.onPress39(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                SchoolSignUp
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>

                              <TouchableOpacity onPress = {()=>this.onPress40(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                SelectDOB
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>

                              <TouchableOpacity onPress = {()=>this.onPress41(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                SelectHeight
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>

                              <TouchableOpacity onPress = {()=>this.onPress42(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                SelectWeight
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>

                              <TouchableOpacity onPress = {()=>this.onPress43(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                TrackActivity1
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>

                              <TouchableOpacity onPress = {()=>this.onPress44(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                TrackActivity2
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>

                              <TouchableOpacity onPress = {()=>this.onPress45(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                TrackActivity3
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>

                              <TouchableOpacity onPress = {()=>this.onPress46(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                TrackActivity4
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>

                              <TouchableOpacity onPress = {()=>this.onPress47(navigation )}>
                              <Text  style={styles.tuesday_heading}>
                                TrackActivity5
                              </Text>
                              </TouchableOpacity>

                              <View style={styles.whiteline}></View>
                          </View>
                      </View>
              </ScrollView>
          </View>



    );
  }
}
