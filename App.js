/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Switch,
  Image,
  PixelRatio,
  KeyboardAvoidingView,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  TextInput

} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import Icon from 'react-native-vector-icons/FontAwesome';
import { StackNavigator } from 'react-navigation';


const Dimensions = require('Dimensions');
const {height, width} = Dimensions.get('window');


import Home from './source/components/home.js'
import SignUp from './source/components/signup.js'
import SignIn from './source/components/signin.js'
import Workout from './source/components/workout.js'
import IndexPage from './source/components/indexPage.js'
import Youarefinished from './source/components/youarefinished.js'
import GetNotified from './source/components/getNotified.js'
import DevicesConnected from './source/components/devicesConnected.js'
import WorkoutSummary from './source/components/workoutSummary.js'
import UpcomingWorkouts from './source/components/upcomingWorkouts.js'
import Feed from './source/components/feed.js'
import WorkoutDetails from './source/components/workoutDetails.js'
import ConnectedDevices from './source/components/connectedDevices.js'
import SchoolSignUp from './source/components/schoolSignUp.js'
import UserInfo1 from './source/components/userInfo1.js'
import UserInfo2 from './source/components/userInfo2.js'
import UserInfo3 from './source/components/userInfo3.js'
import UserInfo4 from './source/components/userInfo4.js'
import Activity from './source/components/activity.js'
import Calendar from './source/components/calendar.js'
import Comments from './source/components/comments.js'
import Filter from './source/components/filter.js'
import FindFriends from './source/components/findFriends.js'
import Menu from './source/components/menu.js'
import Notifications from './source/components/notifications.js'
import PickAnExercise from './source/components/pickAnExercise.js'
import PickCourse1 from './source/components/pickCourse1.js'
import PickCourse2 from './source/components/pickCourse2.js'
import PickCourse3 from './source/components/pickCourse3.js'
import PickCourse4 from './source/components/pickCourse4.js'
import Profile1 from './source/components/profile1.js'
import Profile2 from './source/components/profile2.js'
import Profile3 from './source/components/profile3.js'
import Profile4 from './source/components/profile4.js'
import ProfileDetails1 from './source/components/profileDetails1.js'
import ProfileDetails2 from './source/components/profileDetails2.js'
import ProfileDetails3 from './source/components/profileDetails3.js'
import ProfileDetails4 from './source/components/profileDetails4.js'
import PickSchool from './source/components/pickSchool.js'

import SelectDOB from './source/components/selectDOB.js'
import SelectHeight from './source/components/selectHeight.js'
import SelectWeight from './source/components/selectWeight.js'
import TrackActivity1 from './source/components/trackActivity1.js'
import TrackActivity2 from './source/components/trackActivity2.js'
import TrackActivity3 from './source/components/trackActivity3.js'
import TrackActivity4 from './source/components/trackActivity4.js'
import TrackActivity5 from './source/components/trackActivity5.js'
import TrackActivity6 from './source/components/trackActivity6.js'



import AllComponents from './source/components/allComponents.js'
import AreaChartExample from './source/components/graph.js'

console.disableYellowBox = true;

const App = StackNavigator({

  // Home: { screen: Home },
  SignIn : {screen : SignIn},
  SignUp : { screen : SignUp},
  Workout : { screen : Workout},
  IndexPage : { screen : IndexPage},
  Youarefinished : { screen : Youarefinished},
  GetNotified : { screen : GetNotified},
  DevicesConnected : { screen : DevicesConnected},
  WorkoutSummary : { screen : WorkoutSummary},
  UpcomingWorkouts : { screen : UpcomingWorkouts},
  Feed : { screen : Feed},
  WorkoutDetails : { screen : WorkoutDetails},
  ConnectedDevices : { screen : ConnectedDevices},
  SchoolSignUp : { screen : SchoolSignUp},
  UserInfo1 : { screen : UserInfo1},
  UserInfo2 : { screen : UserInfo2},
  UserInfo3 : { screen : UserInfo3},
  UserInfo4 : { screen : UserInfo4},
  Activity : { screen : Activity },
  Calendar : { screen : Calendar},
  Comments : { screen : Comments},
  Filter : { screen : Filter},
  FindFriends : { screen : FindFriends},
  Menu : { screen : Menu},
  Notifications : { screen :Notifications },
  PickAnExercise  : { screen : PickAnExercise},
  PickCourse1 : { screen : PickCourse1},
  PickCourse2 : { screen : PickCourse2},
  PickCourse3 : { screen : PickCourse3},
  PickCourse4 : { screen : PickCourse4},
  Profile1 : { screen : Profile1},
  Profile2 : { screen : Profile2},
  Profile3 : { screen : Profile3},
  Profile4 : { screen : Profile4},
  ProfileDetails1 : { screen : ProfileDetails1 },
  ProfileDetails2 : { screen : ProfileDetails2},
  ProfileDetails3 : { screen : ProfileDetails3},
  ProfileDetails4 : { screen : ProfileDetails4},
  PickSchool : { screen : PickSchool},


  SelectDOB : { screen : SelectDOB},
  SelectHeight : { screen : SelectHeight},
  SelectWeight : { screen : SelectWeight},
  TrackActivity1 : { screen : TrackActivity1},
  TrackActivity2 : { screen : TrackActivity2},
  TrackActivity3 : { screen : TrackActivity3},
  TrackActivity4 : { screen : TrackActivity4},
  TrackActivity5 : { screen : TrackActivity5},
  TrackActivity6 : { screen : TrackActivity6},

  AllComponents : { screen : AllComponents},
  AreaChartExample : { screen : AreaChartExample}

},{
  initialRouteName: 'IndexPage',
  headerMode: 'none',
});

export default App
